<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('partial.index');
});

Route::resource('postingan', 'PostinganController');

Route::resource('kategori', 'KategoriController')->only([
    'index', 'update', 'create', 'store'
]);