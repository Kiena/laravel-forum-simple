@extends('partial.index')

@push('style')
    <!-- Custom styles for this page -->
    <link href="{{asset('lib/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endpush

@section('heading')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Kategori</h1>
    <a href="/kategori/create" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i
            class="fas fa-plus-square fa-sm text-white"></i> Create New Category</a>
</div>
@endsection

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Category name</th>
                        <th>Latest update</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($kategori as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->kategori}}</td>
                        <td>{{$value->updated_at}}</td>
                        <td>{{$value->created_at}}</td>
                    </tr>
                    @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                    @endforelse              
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

        {{-- <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">www</th>
                    
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($genre as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->nama}}</td>
                        <td>
                            <form action="/genre/{{$value->id}}" method="POST">
                            @csrf
                                
                            @method('DELETE')
                            <a href="/genre/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/genre/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <input type="submit" class="btn btn-danger my-1" onclick="return confirm('Apakah anda yakin?')" value="Delete">

                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table> --}}

@push('script')
    <!-- Page level plugins -->
    <script src="{{asset('lib/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('lib/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('lib/js/demo/datatables-demo.js')}}"></script>
@endpush