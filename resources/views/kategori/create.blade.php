@extends('partial.index')

@section('header-card')
    
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Total (Category) </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$kategori->count()}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('content')
<!-- Content Column -->
    <div class="card shadow mb-4 col-lg-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-success">Illustrations</h6>
        </div>
        <div class="card-body">
            <form class="user" action="/kategori" method="POST">
                @csrf
                <div class="form-group">
                    <input type="text" name="kategori" class="form-control form-control-user" placeholder="Enter Category...">
                </div>
                @error('kategori')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

                <button type="submit" class="btn btn-success btn-user btn-block">
                    Tambah
                </button>     
            </form>
        </div>
    </div>    
@endsection